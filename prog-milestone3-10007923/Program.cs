﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_10007923
{
    public class Program
    {
        private static List<Pizza> pizzas = new List<Pizza>();
        private static List<Drink> drinks = new List<Drink>();

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello, what's your name?");
            string name = Console.ReadLine();
            Console.WriteLine("and what is your contact number?");
            string number = Console.ReadLine();

            bool running = true;
            while (running)
            {
                Console.WriteLine("Would you like to order a pizza?");
                Console.WriteLine("Please enter 1 for yes");
                Console.WriteLine("Please enter 2 for no");
                int pizzaselect = 0;
                while (!int.TryParse(Console.ReadLine(), out pizzaselect))
                    Console.WriteLine("Please enter a correct number, try again");
                Console.Clear();
                if (pizzaselect == 1)
                {
                    bool orderingPizza = true;
                    do
                    {
                        string typeInput = getTypeInput();
                        Tuple<string, double> detailsInput = getDetailsInput();
                        Pizza ordered = new Pizza();
                        ordered.details = detailsInput;
                        ordered.type = typeInput;
                        pizzas.Add(ordered);
                        var answer = askIfTheyWantMorePizzas();
                        if (answer == 1)
                        {
                            orderingPizza = true;
                        }
                        else if (answer == 2)
                        {
                            orderingPizza = false;
                        }
                        
                    } while (orderingPizza);
                }
                Console.WriteLine("Would you like to order drinks?");
                Console.WriteLine("Please enter 1 for yes");
                Console.WriteLine("Please enter 2 for no");
                var theyWantDrinks = 0;
                while (!int.TryParse(Console.ReadLine(), out theyWantDrinks))
                    Console.WriteLine("Please enter a correct number, try again");
                Console.Clear();
                if (theyWantDrinks == 1)
                {
                    bool orderingDrinks = true;
                    do
                    {
                        string drinkInput = getDrinkInput();
                        Drink ordered = new Drink();
                        ordered.drink = drinkInput;
                        drinks.Add(ordered);
                        var answer = askIfTheyWantMoreDrinks();
                        if (answer == 1)
                        {
                            orderingDrinks = true;
                        }
                        else if (answer == 2)
                        {
                            orderingDrinks = false;
                        }
                    } while (orderingDrinks);
                }
                running = finished(doneRunning());              
                
            }
      
            Console.WriteLine("Would you like to pay cash or eftpos today?");
            Console.WriteLine("Please enter 1 for cash or 2 for eftpos");
            int cashOrEftpos = 0;
            while (!int.TryParse(Console.ReadLine(), out cashOrEftpos))
                Console.WriteLine("Please enter a correct number, try again");
            Console.Clear();
            if (cashOrEftpos == 1)
            {
                Console.WriteLine("Please enter the total cash amount of which you are paying");
                double paymentAmount = 0;
                while (!double.TryParse(Console.ReadLine(), out paymentAmount))
                    Console.WriteLine("Please enter a correct number, try again");
                double totalcost = cost();
                double change = paymentAmount - totalcost;
                Console.WriteLine($"Your change is {change}");
                Console.WriteLine($"Thank you for ordering {name}");
                Console.WriteLine("Please enter any key to exit");
            }
            else if (cashOrEftpos == 2)
            {
                double totalcost = cost();
                Console.WriteLine("Your change is $0");
                Console.WriteLine($"Thank you for ordering {name}");
                Console.WriteLine("Please enter any key to exit");
            }
            Console.Read();
        }

        public static string getTypeInput()
        {
            PizzaTypeListPrint();
            string pizzaType = string.Empty;
            int pizzaTypeSelection = 0;           
            while (!int.TryParse(Console.ReadLine(), out pizzaTypeSelection))
                Console.WriteLine("Please enter a correct number, try again");
            Console.Clear();
            bool isvalid = true;
            do
            {
                switch (pizzaTypeSelection)

                {
                    case 1:
                        {
                            pizzaType = "Hawaian";
                            break;
                        }
                    case 2:
                        {
                            pizzaType = "Meatlovers";
                            break;
                        }
                    case 3:
                        {
                            pizzaType = "Vegetarian";
                            break;
                        }
                    case 4:
                        {
                            pizzaType = "Cheese";
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Please input a correct value");
                            isvalid = false;
                            break;
                        }
                }
            } while (!isvalid);
            return pizzaType;
        }
        public static Tuple<string, double> getDetailsInput()
        {
            PizzaSizeListPrint();
            string pizzaSize = string.Empty;
            double pizzaPrice = 0;
            int pizzaSizeSelection = 0;
            while (!int.TryParse(Console.ReadLine(), out pizzaSizeSelection))
                Console.WriteLine("Please enter a correct number, try again");
            Console.Clear();
            bool isvalid = true;
            do
            {
                switch (pizzaSizeSelection)
                {
                    case 1:
                        {
                            pizzaSize = "Small";
                            pizzaPrice = 5.00;
                            break;
                        }
                    case 2:
                        {
                            pizzaSize = "Medium";
                            pizzaPrice = 7.50;
                            break;
                        }
                    case 3:
                        {
                            pizzaSize = "Large";
                            pizzaPrice = 10.00;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Please enter a correct value");
                            isvalid = false;
                            break;
                        }
                }
            } while (!isvalid);
            var pizzaDetails = new Tuple<string, double>(pizzaSize, pizzaPrice);
            return pizzaDetails;
        }
        public static string getDrinkInput()
        {
            DrinkTypeListPrint();
            string drinkType = string.Empty;
            int drinkTypeSelection = 0;
            while (!int.TryParse(Console.ReadLine(), out drinkTypeSelection))
                Console.WriteLine("Please enter a correct number, try again");
            Console.Clear();
            bool isvalid = true;
            do
            {
                switch (drinkTypeSelection)
                {
                    case 1:
                        {
                            drinkType = "Mountain Dew";
                            break;
                        }
                    case 2:
                        {
                            drinkType = "Fanta";
                            break;
                        }
                    case 3:
                        {
                            drinkType = "Orange Juice";
                            break;
                        }
                    case 4:
                        {
                            drinkType = "Water";
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Please input a correct value");
                            isvalid = false;
                            break;
                        }

                }
            } while (!isvalid);
            return drinkType;
        }
    

        public static void PizzaSizeListPrint()
        {
            Console.WriteLine("Which size of pizza would you like?");
            Console.WriteLine("Please Enter 1 for Small $5.00");
            Console.WriteLine("Please Enter 2 for Medium $7.50");
            Console.WriteLine("Please Enter 3 for Large $10.00");
        }
        public static void PizzaTypeListPrint()
        {
            Console.WriteLine("Which type of pizza would you like?");
            Console.WriteLine("Please Enter 1 for Hawaian");
            Console.WriteLine("Please Enter 2 for Meatlovers");
            Console.WriteLine("Please Enter 3 for Vegetarian");
            Console.WriteLine("Please Enter 4 for Cheese");
        }
        public static void DrinkTypeListPrint()
        {
            Console.WriteLine("Which drink would you like to order?");
            Console.WriteLine("Please note all drinks are $3.00");
            Console.WriteLine("Please enter 1 for Mountain Dew");
            Console.WriteLine("Please enter 2 for Fanta");
            Console.WriteLine("Please enter 3 for Orange Juice");
            Console.WriteLine("Please enter 4 for Water");
        }
        public static int askIfTheyWantMorePizzas()
        {
            Console.WriteLine("Would you like to order another pizza or proceed to ordering drinks?");
            bool morePizza = true;
            do
            {
                Console.WriteLine("Please enter 1 for ordering another pizza");
                Console.WriteLine("Please enter 2 to proceed to ordering drinks");
                var answer = 0;
                while (!int.TryParse(Console.ReadLine(), out answer))
                    Console.WriteLine("Please enter a correct number, try again");
                Console.Clear();
                if (answer == 1 || answer == 2)
                {
                    morePizza = false;
                    
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Please input a correct value");
                    morePizza = true;
                }
                return answer;
            } while (morePizza);
            
        }
        public static int askIfTheyWantMoreDrinks()
        {
            Console.WriteLine("Would you like to order another drink?");
            bool moreDrinks = true;
            do
            {
                Console.WriteLine("Please enter 1 to add more drinks");
                Console.WriteLine("Please enter 2 to finish ordering drinks");
                var answer = 0;
                while (!int.TryParse(Console.ReadLine(), out answer))
                    Console.WriteLine("Please enter a correct number, try again");
                Console.Clear();
                if (answer == 1 || answer == 2) 
                {
                    moreDrinks = false;                  
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Please input a correct value");
                    moreDrinks = true;
                }
                return answer;
            } while (moreDrinks);

        }
        public static int doneRunning()
        {
            Console.WriteLine("Would you like to proceed to payment?");
            bool doneRunning = true;
            do
            {
                Console.WriteLine("Please enter 1 to proceed to payment");
                Console.WriteLine("Please enter 2 to order more goods");
                var answer = 0;
                while (!int.TryParse(Console.ReadLine(), out answer))
                    Console.WriteLine("Please enter a correct number, try again");
                Console.Clear();
                if (answer == 1 || answer == 2)
                {
                    doneRunning = false;

                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Please input a correct value");
                    doneRunning = true;
                }
                return answer;
            } while (doneRunning);
        }

        public static bool finished(int answer)
        {
                int finished = answer;
                bool finishedOrdering = false;
                if (finished == 1)
                {
                    finishedOrdering = false;
                }
                else if (finished == 2)
                {
                    finishedOrdering = true;
                }
                return finishedOrdering;
        }
        public static double cost()
        {
            double totalCost = 0;
            Console.WriteLine("You have ordered");
            foreach (Pizza element in pizzas)
            {
                Console.WriteLine($"{element.type}, {element.details.Item1}");
                totalCost += element.details.Item2;
            }
            Console.WriteLine("Pizzas");
            Console.WriteLine("You have ordered");
            foreach (Drink element in drinks)
            {
                Console.WriteLine(element.drink);
                totalCost += element.price;
            }
            Console.WriteLine("Drinks");
            Console.WriteLine($"This comes to a total of ${totalCost}");
            return totalCost;
        }

    }
}
    

    public class Pizza
    {
        public string type;
        public Tuple<string, double> details; //size, price      
    }

    public class Drink
    {
        public string drink;
        public double price = 3.00;
    }
